;<?php/*

[general]
name="mailspooler"
version="0.13.6"
addon_type="EXTENSION"
delete=1
db_prefix="mailspooler_"
encoding="UTF-8"
mysql_character_set_database="latin1,utf8"
description="mail spooler"
description.fr="Module fournissant une liste de traitement des envois d'emails"
long_description.fr="README.md"
author="Paul de Rosanbo (CANTICO)"
ov_version="8.4.92"
php_version="5.1.0"
mysql_version="4.1.2"
addon_access_control="0"
icon="internet-mail.png"
configuration_page="main"
tags="extension,mail,default"
[addons]
widgets="1.0.49"
LibOrm="0.11.2"

;*/?>
