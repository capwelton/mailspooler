<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



function mailspooler_upgrade($version_base,$version_ini)
{
	include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
	include_once $GLOBALS['babInstallPath']."utilit/path.class.php";
	include_once $GLOBALS['babInstallPath']."utilit/devtools.php";
	
	global $babDB;
	
	$addon = bab_getAddonInfosInstance('mailspooler');
	
	$addon->removeAllEventListeners();
	
	$addon->addEventListener('bab_eventBeforeMailSent'		, 'mailspooler_onBeforeMailSent'	, 'events.php');
	$addon->addEventListener('bab_eventAfterMailSent'		, 'mailspooler_onAfterMailSent'	    , 'events.php');
	$addon->addEventListener('LibTimer_eventEvery5Min'		, 'mailspooler_onEvery5Min'	        , 'events.php');
	$addon->addEventListener('LibTimer_eventEvery30Min'		, 'mailspooler_onEvery30Min'	    , 'events.php');
	
	

	$tables = new bab_synchronizeSql($addon->getPhpPath().'dump.sql');
	
	

	$addon->registerFunctionality('Mailspooler', 'mailspooler.class.php');
	
	if (!bab_isTableField('mailspooler_mail', 'send_inprogress'))
	{
		$babDB->db_query("ALTER TABLE mailspooler_mail ADD `send_inprogress` tinyint(1) unsigned default 0 COMMENT '0=No send, 1=Send inprogress'");
	}
	
	return true;
}






function mailspooler_onDeleteAddon()
{
	$addon = bab_getAddonInfosInstance('mailspooler');
	
	$addon->removeAllEventListeners();
	$addon->unregisterFunctionality('Mailspooler');

	return true;
}



