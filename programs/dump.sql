
CREATE TABLE `mailspooler_mail` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `mail_hash` varchar(255) NOT NULL default '',
  `mail_subject` varchar(255) NOT NULL default '',
  `body` text NOT NULL,
  `altbody` text NOT NULL,
  `format` varchar(32) NOT NULL default '',
  `recipients` text NOT NULL,
  `mail_data` text NOT NULL,
  `sent_status` tinyint(1) unsigned default NULL COMMENT 'null=waiting, 0=failed, 1=sent',
  `smtp_trace` text NOT NULL,
  `error_msg` text NOT NULL,
  `mail_date` datetime NOT NULL,
  `send_inprogress` tinyint(1) unsigned default 0 COMMENT '0=No send, 1=Send inprogress',
  PRIMARY KEY  (`id`),
  KEY `mail_date` (`mail_date`),
  KEY `mail_hash` (`mail_hash`)
);