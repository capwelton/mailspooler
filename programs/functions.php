<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * 
 * @param string $str
 * @return string
 */
function mailspooler_translate($str)
{
	return bab_translate($str, 'mailspooler');
}


function mailspooler_keepSentMail()
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/mailspooler/');
	return $registry->getValue('keepSentMail', false);
}


/**
 * @return mailspooler_Controller
 */
function mailspooler_Controller()
{
    require_once dirname(__FILE__) . '/controller.class.php';
    return bab_getInstance('mailspooler_Controller');
}



function mailspooler_arrayToString($arr)
{
    $return = array();

    foreach($arr as $recipient) {
        if (!empty($recipient[1])) { /* name of recipient */
            $return[] = $recipient[1] . ' <'.$recipient[0].'>';
            	
        } else {
            $return[] = $recipient[0];
        }
    }

    return implode(', ', $return);
}

/**
 * Add context menu to page
 * @param babBody|Widget_Page $page
 */
function mailspooler_ContextMenu($page, $currentPage)
{
    $page->addItemMenu('editlist', mailspooler_translate('Delete or send spooled emails'), '?tg=addon/mailspooler/mailspool');
    $page->addItemMenu('search', mailspooler_translate('Search emails'), mailspooler_Controller()->Mail()->displayList()->url());
    $page->addItemMenu('configuration', mailspooler_translate('Mail spooler configuration'), mailspooler_Controller()->Configuration()->edit()->url());
    $page->setCurrentItemMenu($currentPage);
}

