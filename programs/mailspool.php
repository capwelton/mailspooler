<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';

function mailspool()
{
	global $babBody;
	class temp
		{
		var $altbg = true;

		function temp()
			{
			$this->t_mail_subject = mailspooler_translate("Mail subject");
			$this->t_mail_date = mailspooler_translate("Date");
			$this->t_send = mailspooler_translate("Send");
			$this->t_delete = mailspooler_translate("Delete");
			$this->checkall = mailspooler_translate("Check all");
			$this->uncheckall = mailspooler_translate("Uncheck all");
			$this->t_waiting = mailspooler_translate("Wait for sending");

			$this->db = $GLOBALS['babDB'];
			

			$this->res = $this->db->db_query("
			SELECT 
				id,
				mail_subject, 
				UNIX_TIMESTAMP(mail_date) mail_date,
				error_msg, 
				sent_status,
			    recipients  
			FROM mailspooler_mail 
				ORDER BY mail_date DESC
				");
			$this->count = $this->db->db_num_rows($this->res);
			}

		function getnext()
			{
			if ($arr = $this->db->db_fetch_assoc($this->res)) {
				$this->id = $arr['id'];
				$this->altbg = !$this->altbg;
				$this->mail_subject = bab_toHtml($arr['mail_subject']);
				$this->mail_date = bab_toHtml(bab_longDate($arr['mail_date']));
				$this->error_msg = bab_toHtml($arr['error_msg']);
				$this->waiting = null === $arr['sent_status'];
				$this->recipients = bab_toHtml($arr['recipients']);
				return true;
			}
			return false;
		}
	}
	
	$addon = bab_getAddonInfosInstance('mailspooler');

	$temp = new temp();
	$babBody->babecho($addon->printTemplate($temp, "mailspool.html", "list"));
}













function send_checked_mail() {

	global $babBody;
	include_once $GLOBALS['babInstallPath']."utilit/mailincl.php";
	require_once dirname(__FILE__) . '/set/mail.class.php';

	$db = $GLOBALS['babDB'];
	$mail = bab_rp('mail', false);
	if ($mail) {

		$mail_obj = bab_mail();

		if (!$mail_obj) {
			$babBody->msgerror = mailspooler_translate("Mail is not configured");
			return false;
		}

		$res = $db->db_query("
			SELECT * FROM mailspooler_mail WHERE id IN(".$db->quote($mail).")
		");
		
		$spooler = new mailspooler_mail;
		
		try {

			while ($arr = $db->db_fetch_assoc($res)) {
				$spooler->sendMail($mail_obj, $arr);
			}
		
		} catch(ErrorException $e)
		{
			$babBody->addError($e->getMessage());
		}
	}
}



function delete_checked_mail() {
	global $babDB;
	require_once dirname(__FILE__) . '/set/mail.class.php';

	$mail = bab_rp('mail', false);
	$spooler = new mailspooler_mail;
	
	if ($mail) {
	
		$res = $babDB->db_query('
			SELECT * FROM mailspooler_mail WHERE id IN('.$babDB->quote($mail).')
		');
		
		while ($arr = $babDB->db_fetch_assoc($res)) {
			$spooler->deleteEmail($arr);
		}

		
	}
}






function mailspooler_download()
{
	global $babDB;
	
	$email = bab_rp('email');
	$file_crc = (int) bab_rp('file');
	
	$res = $babDB->db_query("SELECT * FROM mailspooler_mail WHERE id =".$babDB->quote($email)."");
	$arr = $babDB->db_fetch_assoc($res);
	$mail_data = unserialize($arr['mail_data']);
	

	
	foreach($mail_data['files'] as $file) {
		if (is_file($file[0]))
		{
			$crc = abs(crc32($file[0]));
			if ($file_crc === $crc)
			{
				bab_downloadFile(new bab_Path($file[0]), $file[1], false, true);
			}
		}
	}
}







/* main */

if( !bab_isUserAdministrator() )
{
	$babBody->addError(mailspooler_translate('Access denied'));
	return;
}

$idx = bab_rp('idx', 'list');
$item = bab_rp('item', 1); /* id site */

if (isset($_POST['send'])) {
	bab_requireSaveMethod() && send_checked_mail();
}

if (isset($_POST['delete'])) {
	bab_requireDeleteMethod() && delete_checked_mail();
}


switch($idx) {
		
	case 'download':
		mailspooler_download();
		break;


	case 'list': // edit email in list
		mailspool();
		$babBody->setTitle(mailspooler_translate("Batch processing of undelivered mails"));
		mailspooler_ContextMenu($babBody, 'editlist');
		break;
}

$babBody->setCurrentItemMenu($idx);
