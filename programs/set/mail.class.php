<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/mailincl.php';
require_once dirname(__FILE__) . '/../functions.php';

$Orm = bab_functionality::get('LibOrm');
/*@var $Orm Func_LibOrm */
$Orm->initMySql();

$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
ORM_MySqlRecordSet::setBackend($mysqlbackend);



/**
 * @property ORM_PkField        $id
 * @property ORM_StringField    $mail_hash
 * @property ORM_StringField    $mail_subject
 * @property ORM_TextField      $body
 * @property ORM_TextField      $altbody
 * @property ORM_StringField    $format
 * @property ORM_TextField      $recipients
 * @property ORM_TextField      $mail_data
 * @property ORM_IntField       $sent_status
 * @property ORM_TextField      $smtp_trace
 * @property ORM_TextField      $error_msg
 * @property ORM_DateTimeField  $mail_date
 * @property ORM_IntField       $send_inprogress
 */
class mailspooler_MailSet extends ORM_MySqlRecordSet
{
	public function __construct()
	{
		parent::__construct();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('mail_hash'),
		    ORM_StringField('mail_subject'),
		    ORM_TextField('body'),
		    ORM_TextField('altbody'),
		    ORM_StringField('format'),
		    ORM_TextField('recipients'),
		    ORM_TextField('mail_data'),
		    ORM_IntField('sent_status'),
		    ORM_TextField('smtp_trace'),
		    ORM_TextField('error_msg'),
		    ORM_DateTimeField('mail_date'),
		    ORM_IntField('send_inprogress')
		);

	}

}



/**
 * @property int        $id
 * @property string     $mail_hash
 * @property string     $mail_subject
 * @property string     $body
 * @property string     $altbody
 * @property string     $format
 * @property string     $recipients
 * @property string     $mail_data
 * @property int        $sent_status
 * @property string     $smtp_trace
 * @property string     $error_msg
 * @property string     $mail_date
 * @property int        $send_inprogress
 */
class mailspooler_Mail extends ORM_MySqlRecord
{


    public function __construct(ORM_RecordSet $oParentSet = null)
    {
        if (isset($oParentSet)) {
            parent::__construct($oParentSet);
        }
    }



    public function getStatusText()
    {
        switch ($this->sent_status)
        {
            case null:	$status = mailspooler_translate('Wait for sending');	break;
            case '0':	$status	= mailspooler_translate('Failed');				break;
            case '1':	$status	= mailspooler_translate('Sent');				break;
            default:    $status	= mailspooler_translate('Unknown status');		break;
        }

        return $status;
    }


	private function addMail(&$mail, $list) {
		foreach($list as $arr) {
			$mail[] = $arr[0];
		}
	}

	/**
	 * Record mail in database from event mail infos
	 * @return string
	 */
	public function recordMail(bab_eventMail $event) {
		global $babDB;
		$addon = bab_getAddonInfosInstance('mailspooler');

		$attachments = array();

		if ($event->attachements) {

			$attachments = $event->attachements;

			$dir = new bab_Path($addon->getUploadPath().'mail/');
			$dir->createDir();


			foreach($event->attachements as $k => $arr) {
				$newname = clone $dir;
				$newname->push(md5(uniqid(rand(), true)));
				if (is_file($arr[0])) {
					copy($arr[0], $newname->tostring());
					$attachments[$k][0] = $newname->tostring();
				}
			}
		}

		$imageattachments = array();

		if (isset($event->imageattachements) && $event->imageattachements) {

			$imageattachments = $event->imageattachements;

			$dir = new bab_Path($addon->getUploadPath().'mail/');
			$dir->createDir();


			foreach($event->imageattachements as $k => $arr) {
				$newname = clone $dir;
				$newname->push(md5(uniqid(rand(), true)));
				if (is_file($arr[0])) {
					copy($arr[0], $newname->tostring());
					$imageattachments[$k][0] = $newname->tostring();
				}
			}
		}

		$recipients = array();
		$this->addMail($recipients, $event->to);
		$this->addMail($recipients, $event->cc);
		$this->addMail($recipients, $event->bcc);

		$recipients = implode(', ',$recipients);

		$data = array(
				'from'		=> $event->from,
				'sender'	=> $event->sender,
				'to'		=> $event->to,
				'cc'		=> $event->cc,
				'bcc'		=> $event->bcc,
				'files'		=> $attachments,
				'imagefiles'=> $imageattachments
			);

		$data = serialize($data);

		$smtp_trace = isset($event->smtp_trace) ? $event->smtp_trace : '';

		if ($event instanceof bab_eventAfterMailSent)
		{
			$sent_status = $event->sent_status ? '1' : '0';
			$ErrorInfo = (string) $event->ErrorInfo;

		} else {
			$sent_status = null;
			$ErrorInfo = '';
		}

		$mail_hash = md5($event->subject.$event->body.$data);

		$res = $babDB->db_query("SELECT id FROM mailspooler_mail WHERE mail_hash='".$babDB->db_escape_string($mail_hash)."'");

		if (0 < $babDB->db_num_rows($res)) {

			// ignorer le principe du hash car cela empeche de renvoyer un mail a partir de la liste des mails non envoyes
			// le hash est le meme et il ne l'ajoute pas

			bab_debug("mail creation in spooler rejected because mail_hash allready exists : ".$mail_hash);
			return $mail_hash;
		}

		$babDB->db_query("INSERT INTO mailspooler_mail
				( mail_hash, mail_subject, body, altbody, format, recipients, mail_data, sent_status, smtp_trace, error_msg, mail_date )
			VALUES
				(
					".$babDB->quote($mail_hash).",
					".$babDB->quote($event->subject).",
					".$babDB->quote($event->body).",
					".$babDB->quote($event->altBody).",
					".$babDB->quote($event->format).",
					".$babDB->quote($recipients).",
					".$babDB->quote($data).",
					".$babDB->quoteOrNull($sent_status).",
					".$babDB->quote($smtp_trace).",
					".$babDB->quote($ErrorInfo).",
					NOW()
				)
			");

		return $mail_hash;
	}



	public function getNextSendMail()
	{
		global $babDB;

		$babDB->db_query("LOCK TABLES mailspooler_mail WRITE");

		$res = $babDB->db_query("
			SELECT * FROM mailspooler_mail WHERE sent_status IS NULL AND send_inprogress = 0 LIMIT 0,1
		");

		$arr = $babDB->db_fetch_assoc($res);

		if(!$arr){
			$babDB->db_query("UNLOCK TABLES");
			return false;
		}

		$babDB->db_query("
			UPDATE mailspooler_mail SET
				send_inprogress='1'
			WHERE
				id=".$babDB->quote($arr['id'])
		);

		$babDB->db_query("UNLOCK TABLES");

		return $arr;
	}


	/**
	 * Process waiting mails
	 * @throws ErrorException
	 * @return int number of processed email
	 */
	public function sendWaiting()
	{
		include_once $GLOBALS['babInstallPath']."utilit/mailincl.php";

		global $babDB;

		$mail_obj = bab_mail();

		if (!$mail_obj) {
			throw new ErrorException(mailspooler_translate("Mail is not configured"));
		}

		$spooler = new mailspooler_mail;
		$n = 0;

		while ($arr = $this->getNextSendMail()) {
			if ($spooler->sendMail($mail_obj, $arr))
			{
				$n++;
			}
		}

		return $n;
	}




    /**
     * Try to resend failed send mail
     * @throws ErrorException
     * @return int number of processed email
     */
    public function reSend()
    {
        include_once $GLOBALS['babInstallPath']."utilit/mailincl.php";

        global $babDB;

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/mailspooler/');
        $resendNumber = $registry->getValue('resendNumber', '40');

        if ($resendNumber <= 0) {
            return 0;
        }

        $mail_obj = bab_mail();

        if (!$mail_obj) {
            throw new ErrorException(mailspooler_translate("Mail is not configured"));
        }


        $res = $babDB->db_query("
        	SELECT * FROM mailspooler_mail WHERE sent_status = '0' AND DATE_ADD(mail_date, INTERVAL 12 HOUR)>=NOW() LIMIT 0, " . $babDB->db_escape_string($resendNumber)
        );

        $spooler = new mailspooler_Mail();
        $n = 0;

        while ($arr = $babDB->db_fetch_assoc($res)) {
            if (!$spooler->sendMail($mail_obj, $arr)) {
                break;
            }
            $n++;
        }

        return $n;
    }


	private function addRecipient(babMail $mail, $type, $arr) {
		$function = 'mail'.$type;

		foreach($arr as $recipient) {
			if (isset($recipient[1])) { /* name of recipient */
				/* Add email only if it's not empty */
				if (!empty($recipient[0])) {
					$mail->$function($recipient[0], $recipient[1]);
				}
			} else {
				/* Add email only if it's not empty */
				if (!empty($recipient[0])) {
					$mail->$function($recipient[0]);
				}
			}
		}
	}


	/**
	 *
	 * @param babMail $mail_obj
	 * @param array $arr
	 * @return unknown_type
	 */
	public function initMail(babMail $mail_obj, Array $arr)
	{


		$mail_obj->clearAllRecipients();
		$mail_obj->clearReplyTo();

		$data = unserialize($arr['mail_data']);

		if (isset($data['from'])) {
			if (isset($data['from'][1])) {
				$mail_obj->mailFrom($data['from'][0], $data['from'][1]);
			} else {
				$mail_obj->mailFrom($data['from'][0]);
			}
		}

		$mail_obj->mailSender($data['sender']);

		$this->addRecipient($mail_obj, 'To', $data['to']);
		$this->addRecipient($mail_obj, 'Cc', $data['cc']);
		$this->addRecipient($mail_obj, 'Bcc', $data['bcc']);

		$mail_obj->mailSubject($arr['mail_subject']);
		$mail_obj->mailBody($arr['body'], $arr['format']);
		$mail_obj->mailAltBody($arr['altbody']);

		foreach($data['files'] as $file) {
			if (is_file($file[0]))
			{
				$mail_obj->mailFileAttach($file[0], $file[1], $file[2]);
			}
		}

		if(isset($data['imagefiles'])){
			foreach($data['imagefiles'] as $image) {
				if (is_file($image[0]))
				{
					$mail_obj->mailEmbeddedImage($image[0], $image[1], $image[2], $image[3], $image[4]);
				}
			}
		}

		$mail_obj->hash = $arr['mail_hash'];
	}



	/**
	 * Send a mail in table
	 * @throws ErrorException
	 *
	 * @param	babMail	$mail_obj
	 * @param 	Array	$arr
	 * @return unknown_type
	 */
	public function sendMail(babMail $mail_obj, Array $arr)
	{
		global $babDB;
		$this->initMail($mail_obj, $arr);

		// call Send() without events

		$sent_status = $mail_obj->mail->Send();
		$smtp_trace = isset($mail_obj->mail->smtp_trace) ? $mail_obj->mail->smtp_trace : '';

		if (!$sent_status) {

			$babDB->db_query("
				UPDATE mailspooler_mail SET
					sent_status='0',
					error_msg=".$babDB->quote($mail_obj->ErrorInfo()).",
					smtp_trace=".$babDB->quote($smtp_trace)."
				WHERE
					id=".$babDB->quote($arr['id'])
			);

			throw new ErrorException(mailspooler_translate("Mail server error"));
			return false;
		}

		require_once $GLOBALS['babInstallPath']."utilit/mailincl.php";
		$event = new bab_eventAfterMailSent;
		$event->setMailInfos($mail_obj);
		$event->sent_status = $sent_status;
		$event->ErrorInfo = empty($mail_obj->mail->ErrorInfo) ? null : $mail_obj->mail->ErrorInfo;
		$event->smtp_trace = $smtp_trace;

		bab_fireEvent($event);

		$mail_obj->mailClearAttachments();
		$this->deleteEmail($arr);
		return true;
	}




	/**
	 * Delete mail from table
	 * @param array $arr
	 * @return unknown_type
	 */
	public function deleteEmail(Array $arr) {

	    global $babDB;

	    $data = unserialize($arr['mail_data']);

	    foreach($data['files'] as $file) {
	        if (is_file($file[0]) && is_writable($file[0])) {
	            unlink($file[0]);
	        }
	    }
	    if(isset($data['imagefiles'])){
	        foreach($data['imagefiles'] as $file) {
	            if (is_file($file[0]) && is_writable($file[0])) {
	                unlink($file[0]);
	            }
	        }
	    }

	    $babDB->db_query('
			DELETE FROM mailspooler_mail WHERE id='.$babDB->quote($arr['id']).'
		');
	}



	/**
	 * Sends the mail.
	 * @throws ErrorException
	 *
	 * @return bool
	 */
	public function send()
	{
	    $babMail = bab_mail();

	    if (!$babMail) {
	        throw new ErrorException(mailspooler_translate('Mail is not configured'));
	    }


	    $babMail->clearAllRecipients();
	    $babMail->clearReplyTo();

	    $data = unserialize($this->mail_data);

	    if (isset($data['from'])) {
	        if (isset($data['from'][1])) {
	            $babMail->mailFrom($data['from'][0], $data['from'][1]);
	        } else {
	            $babMail->mailFrom($data['from'][0]);
	        }
	    }

	    $babMail->mailSender($data['sender']);

	    $this->addRecipient($babMail, 'To', $data['to']);
	    $this->addRecipient($babMail, 'Cc', $data['cc']);
	    $this->addRecipient($babMail, 'Bcc', $data['bcc']);

	    $babMail->mailSubject($this->mail_subject);
	    $babMail->mailBody($this->body, $this->format);
	    $babMail->mailAltBody($this->altbody);

	    foreach ($data['files'] as $file) {
	        if (is_file($file[0])) {
	            $babMail->mailFileAttach($file[0], $file[1], $file[2]);
	        }
	    }

	    if (isset($data['imagefiles'])) {
	        foreach ($data['imagefiles'] as $image) {
	            if (is_file($image[0])) {
	                $babMail->mailEmbeddedImage($image[0], $image[1], $image[2], $image[3], $image[4]);
	            }
	        }
	    }

	    $babMail->hash = $this->mail_hash;


	    // call Send() without events

	    $sent_status = $babMail->mail->Send();
	    $smtp_trace = isset($babMail->mail->smtp_trace) ? $babMail->mail->smtp_trace : '';

	    if (!$sent_status) {

	        $this->error_msg = $babMail->ErrorInfo();
	        $this->smtp_trace = $smtp_trace;
	        $this->save();

	        throw new ErrorException(mailspooler_translate("Mail server error"));
	    }

	    require_once $GLOBALS['babInstallPath']."utilit/mailincl.php";
	    $event = new bab_eventAfterMailSent;
	    $event->setMailInfos($babMail);
	    $event->sent_status = $sent_status;
	    $event->ErrorInfo = empty($babMail->mail->ErrorInfo) ? null : $babMail->mail->ErrorInfo;
	    $event->smtp_trace = $smtp_trace;

	    bab_fireEvent($event);

	    $babMail->mailClearAttachments();

	    return true;
	}




	/**
	 * Deletes the mail record and associated data.
	 */
	public function delete()
	{
	    $data = unserialize($this->mail_data);

	    foreach ($data['files'] as $file) {
	        if (is_file($file[0]) && is_writable($file[0])) {
	            unlink($file[0]);
	        }
	    }
	    if (isset($data['imagefiles'])){
	        foreach( $data['imagefiles'] as $file) {
	            if (is_file($file[0]) && is_writable($file[0])) {
	                unlink($file[0]);
	            }
	        }
	    }

	    $set = $this->getParentSet();
	    $set->delete($set->id->is($this->id));
	}

}