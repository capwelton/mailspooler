## Gestion d'une liste d'attente pour les emails à envoyer ##

Liste de traitement des expéditions de mail. Cette liste est composée des mails en attente d'envoi et des mails dont l'envoi à échoué. Il est également possible de conserver les mails envoyés pour conserver une trace.

Lorsque ce module est couplé avec le module LibTimer, les mails dont l'envoi n'est pas encore effectif seront automatiquement relancés. Il est également possible de désactiver l'envoi immédiat et d'effectuer à intervalle régulier l'envoi d'un lot de mails en attente. Cette fonctionalité est notamment utilisée par le module newsletter.